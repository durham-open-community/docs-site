# DevOps stack [WIP]
This is the start of a list of tools used in DevOps. 

## Collaborate

### Application Lifecycle Management (ALM)/Agile
- Trello
- Jira
- Asana

### Communication
- Slack
- HipChat
- RocketChat
- IRC
- Microsoft Teams

### Knowledge sharing
- GitHub Pages
- GitHub Gist
- Mkdocs/Markdown
- Confluence
- Read The Docs
- Reddit
- Graphviz
- GitLab Pages
- Swagger
- Sphinx


### SCM/VCS
- Git
- SVN
- GitHub
- GitLab
- Mercurial
- Gitolite
- Bitbucket

## Build

### Continuous Integration (CI) and Continuous Deployment (CD)
- Jenkins
- Drone.io
- Travis CI
- Bamboo
- GitLab CI
- Circle CI
- TeamCity

### Build
- Gradle
- Grunt
- Maven
- Gulp
- Docker
- Packer
- Apache Ant
- SonarQube

### Database management

## Test

### Testing
- Selenium
- JUnit
- Cucumber
- JMeter
- PyTest
- Unit.net
- jBehave

## Deploy

### Deployment
- Spinnaker

### Configuration management/Infrastructure as Code
- Puppet
- Chef
- Ansible
- Vagrant
- Terraform
- SaltStack
- Cloud Formation

### Artifact Management
- DockerHub
- Quay
- Bower
- Sonatype Nexus
- NPM
- Python Package Index
- Flux
- Helm
- Artifactory

## Run

### Security
- Vault
- Istio
- Clair

### Cloud/Infrastructure as a Service (IaaS)/Platform as a Service (PaaS)
- AWS
- Heroku
- Azure
- CloudFoundary
- GCP
- Openstack

### Orchestration & Scheduling
- Mesosphere
- Kubernetes
- Rancher
- Mesos
- Docker Swarm
- Openshift

### Observability/Monitoring/Logging/Tracing/Analytics
- Logstash
- Hygieia
- Elasticsearch
- DataDog
- Splunk
- Kibana
- Zabbix
- Zipkin
- Prometheus
- Sentry
- New Relic
- Sumo Logic
- Dynatrace
- Grafana
- Atlas
- StatsD
- PagerDuty
- OpsGenie
- Honeycomb.io
- Gremlin 
- ChaosKube