# Thoughts on tech projects
- Understanding the problem to solve is the hardest part of programming, but naming things is harder :)

![Placeholder](https://miro.medium.com/max/556/1*mc8uC4LU1vjhB0Ii1HR8GQ.png)

- Just because something can be extended doesn't mean it should. 
- Versioning is essential.
- Shared knowledge is better than siloed knowledge. 
- It's okay if you can't cover every use case. It better to build simple maintainable code. 
- Containers are really awesome. We should use them.
- Cloud computing is really awesome. We should use it.
- Don't let something that is hard beat you down. Persistence is key.
- Hero culture doesn't work. Heros can become bottle necks. 
- Burnout is real.
- Failure is a part of building technology. It is not a reflection of your abilities.
- Humans use the things we build. Keep that in mind. 
- Human error is a fallacy. 
- I don't know everything. You don't either. We can figure it out together though. 
- Some crazy ideas are really not that crazy.
- Write the code first, keeping in mind runtime and space complexities, and then optimize as needed. 
- Build things with security and observability from the start. 
- Has someone already solved this? Don't reinvent the wheel.
- It's not cool to write unreadable code to keep your job. You are screwing over the next person that is hired to maintain the project. 
- Documentation can be accomplish with well written code in some cases if you assume your audience is someone familiar with the medium of communication. 
- Testing, testing, testing...but finding ways to understand your system gives you insights testing will not. 
- If you are a problem solver then you can build technology solutions. It is not magic. It's just consistant practice. 
