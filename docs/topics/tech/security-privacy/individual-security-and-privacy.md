# Individual Security and Privacy [WIP]

!!!quote
    Security isn't about the tools you use or the software you download - [Electronic Frontier Foundation](https://ssd.eff.org/en/module/your-security-plan)

## Have a plan

Security and privacy require a plan. You should ask yourself the following questions to help you decide how to keep your data and identity safe. 

1. What do I want to protect?
	- Emails
	- Contact list
	- Messages (texts)
	- Your location
	- Your files
	- Devices (malware)
	- Data (searches)
2. Who do I want to protect it (refers to the answer from the first question) from?
	- Bosses
	- Former partners
	- Government
	- Hacker
	- Companies
3. How bad are the consequences if I fail?
	- Devices/data can be held for ransom (ransomware)
	- Malicious actors can use contact data to infiltrate social networks.
4. How likely is it that I will need to protect it (refers to the answer from the first question)?
	- You can understand your level of risk by considering the answers to the questions above and taking the necessary steps to protect information based on those answers. For example, if you are trying to protect private information from your boss then you should limit what you share on social media, avoid using work computers for personal communications or transactions, turn off text and email notifications so they do not pop up on your cell phone lock screen, etc.
5. How much trouble am I willing to go through to try and prevent potential consequences?
	- It takes effort to make things more secure. A risk assessment based on the answers to the first three questions will give you a good idea of what this may look like. Refer to the example in the previous question.

!!!note
    [The Electronic Frontier Foundation has a fantastic resource for people that are just starting their security journey.](https://ssd.eff.org/en) 


## Connecting to the internet? Use a VPN.
VPN stands for virtual private network. It is often used by people to protect their information while using public WiFi and from letting their ISP (internet service provider - Yes, Spectrum can not only spy on you but they can sell your data as well) spy on them. 
### How does it work
VPN's work by creating a secure connection between your device (laptop, smartphone, tablet, etc) and the server operated by the VPN provider. Since all your internet traffic is linked to the VPN's server, your computer has the same IP (internet protocol) address (think of IP address as a street address for things on the internet) as the server which helps hide your identity and your location. Think of this like if you had the ability to walk out the front door of a coworker’s house every time you left your house. It would look like you lived at your coworker’s house to people in that neighborhood even though you didn’t actually live there.

## Navigating the internet
### Use DuckDuckGo for search, not Google 
If you want to stay private on the internet, you should definitely not use Google for search. Google collects a lot of data on people using their services to create personalized search. To make searches personalized companies, like Google, use information they gather from you, when you use all their free services, to give you search results they “guess” you will be interested in. They also use that data to advertise to you. Ever get the creepy feeling that someone is listening to you on your laptop’s microphone because you were just talking about something you want to buy with your roommate and then when you log into Facebook and an ad pops up advertising a sale on that exact item? Those are targeted ads. Companies like Google and Facebook use all the data they have on you to make some very good guesses  about what you want. It can feel weird and scary.  

Google also sells your data to other companies so they can sell you things, make decisions about who you are, what you want, and in some cases  assess risk (for example - some auto insurance companies use data they buy from other companies to make decisions about how much of a risk you may be to make decisions auto insurance rates, but don’t provide the details of how they make that risk assessment.)  

[DuckDuckGo](https://duckduckgo.com/) is a search engine built with privacy in mind.

### Use a Tor browser
Simply speaking [Tor](https://www.torproject.org/) is a network and browser package that helps you stay anonymous on the internet. That alone will not be the end of your privacy journey, but [it's a great start](https://gizmodo.com/tor-is-for-everyone-why-you-should-use-tor-1591191905). 

[Brave](https://brave.com/) is another browser that is built for privacy. It does have the ability to use the Tor network. If staying anonymous on the internet is your goal then Tor is far more private and secure. Brave is good for hiding you from your ISP, but not nearly as secure. You risk assessment plan will help you pick which browser best serves your needs.

### Check the permissions of you browser extensions
Many articles talk about using some browser extensions for privacy on the internet. The two most often cited are:

- uBlock Origin
	+ Ad blocking
	+ Content filtering
	+ Pop-up blocker
- HTTPS Everywhere
	+ Developed collaboratively by EFF and The Tor Project
	+ Automatically makes websites use a more secure HTTPS connection instead of HTTP, if it's supported.

These are great extensions. Often people use other extensions for their password manager, development, shopping, etc. This is why it is so important to make sure you understand what the extension has access to. In a worst case scenario, if the developer does not quickly fix security holes in their software (aka patching vulnerabilities) then the extension can be hijacked for a number of things (e.g. - capture what keys you press on your computer, use your microphone to listen in, use your camera to spy on you, but this mostly depends on what permissions, or access, you give the browser extension)  

### Password security
For security purposes, it is important to have a password that has uppercase and lowercase letters, numbers, special characters (examples: !%$(#&), and spaces if allowed. You can use [this site](https://passwordsgenerator.net/) to generate a password to meet these requirements. You should use a different password for each login you have. Meeting these requirements can make remembering all your passwords difficult. Using a  password manager is the easiest way to accomplish these things.. All you have to remember is the master password.When creating a master password you should follow the advice above. Not following this advice will give someone access to all your accounts if they figure it out.  The two often recommended password managers are:

- 1Password
- LastPass

If you want to make it extra hard for people to get into your account you should set up 2FA (two factor authentication). Many sites support 2FA. It often requires you to set up an email to send the authentication code to or using an app on your phone to access a code that verifies it is you signing on because the code is linked to your phone or email account. This is often referred to as an authentication token and usually is some number of alphanumeric (letters and numbers) or numeric characters, or symbols, you can type on your keyboard.

The important thing to remember is to use a strong password. Don't keep you passwords in a Google doc, Evernote, iPhone notes app, etc. Also consider signing up for [Have I Been Pwned](https://haveibeenpwned.com/). They will show you if your sensitive data is on the dark web and they will notify you if your information shows up in any data breach, if you sign up to be notified. 

### Use common sense
If it seems too good to be true, it probably is. For example, maybe you are trying to save money on textbooks by downloading them from a sketchy looking site. You might then notice that your laptop starts acting strange months later. It's possible that malware was downloaded along with the file and that is causing the strange behavior.

You have to be careful about where you go and what you do on the internet. This means even when you are checking your email. The most important thing you should remember when reviewing an email is to not click on anything until you have decided that the email is safe. It is a good idea to always place your mouse over the email address to see the full address of the sender of the email. Make sure it matches a sender you recognize. If someone gains access to a friend, colleague, or family member’s account they could send you an email asking you to click on a link, in the email, to a funny video or an interesting article. This is an innocent looking way to get someone to install malware on their computer without the person knowing they are doing so.

Emails from businesses, organizations, government agencies (for example - IRS) or anyone outside your contact list should be thoroughly reviewed before replying to or clicking on anything in the email. An email from asking you to verify your username, password, or any sensitive information by clicking a link in an email should be judged with a high level of suspicion. If you feel the email is suspicious at all, call the company and ask if they sent you an email recently requiring you to reset your password or updating sensitive information.
Many email providers have ways of reporting phishing attempts. Phishing is the deceitful practice of sending emails pretending to be from a credible organization to get the person to reveal personal or sensitive information (for example - home address, phone number, social security number, banking information, credit card numbers, etc).
   


## Communication
### Use a messaging app with end to end encryption
End to end encryption is a process where the message you send to someone is made unreadable except by the person on the other end of the message. So if you send someone a text then they are the only one that can read it. Even if someone were to gain access to the message before it got to the person you were sending it to.  

[Signal](https://signal.org/en/) is the best app for this. WhatsApp is very popular outside the US and it uses end to end encryption, but it is owned by Facebook. I think you know where I'm going with this. Just use Signal. It's great. They even have a desktop version.
