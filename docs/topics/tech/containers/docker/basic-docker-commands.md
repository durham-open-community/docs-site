# Basic Docker Commands [WIP]
These are a few simple Docker commands to get you started using Docker. 

## Images
- Pull docker images

```bash
docker pull $image_name:$image_version
```

- List images you have pulled locally

```bash
docker images
```

- Remove a image you have pulled locally

```bash
docker rmi $image_name:$image_version
```

- Build a Docker image (when the `Dockerfile` is in directory you are running the command in)

```bash
docker build --tag $image_name:$image_version .
```

- Build a Docker image using the `--file` flag 
	+ You would use this option if the Dockerfile was in a different directory in your project.

```bash
docker build --tag $image_name:$image_version --flag $file_path
```

## Containers
- List all running and non-running containers

```bash
docker ps -a
```

- Stop a running container
	+ You can get the `$container_id` by running the previous command.

```bash
docker stop $container_id
```

