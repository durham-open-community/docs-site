# Installing Docker
We currently have only a couple of OS we are covering. Will add more as we uncover the process of installing this tool. 

### Mac
If you have `Homebrew` install on you local machine then it's fairly simple. Execute the following in your terminal:
```bash
brew cask install docker
```

### Linux
**RHEL/Fedora/CentOS**
You would execute the following in your terminal:
```bash
yum install -y yum-utils # if you don't have yum-utils installed
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce docker-ce-cli containerd.io
systemctl start docker
```
Then you could run the follow to make sure it is running:
```bash
systemctl status docker.service
```
Although if you are using a RHEL flavor Linux distribution then you really should check out [Podman](https://podman.io/), which in my opinion is why cooler. 

### Windows
I haven't used Windows in sooooooo long, but I'm sure you can just head to this [URL](https://docs.docker.com/docker-for-windows/install/) and it will be pretty easy to follow. The Docker documentation is pretty decent. 

