# Technology jargon [WIP]
Jargon is a set of special words or expressions that are used by a particular group that may be hard for individuals outside the group to understand. I'm hoping to build an living document of these terms to create shared meaning with people outside the tech profession. I will try not to oversimplify in creating shared meaning.

## A
### Abstraction
**_Abstraction_** is a concept in computer science that refers to the idea of reducing complexity by removing unneeded information. For example, when you press a key on your keyboard and a letter shows up on your screen there are a number of complicated things taking place between when you press the key and when the character shows up on the screen. All those complicated things are _abstracted_ away so you don't have to think about it. 

## B
### Backend
The **_backend_** refers to the things that are not seen by the person using the webpage or software. For instance, when you search for something in Google all the information is stored somewhere in the US or even in another country. When you hit enter the information is retreived and so you can see it. That information was being stored on the _backend_ of the website. 

## C
### Character
A **_character_** is any single letter, number, whitespace, or symbol that can represented on computer screen by a keyboard. 

## D
### Daemon
A **_daemon_** is a process running the background that intiates an action based on some sort of input.

## E
### Encoding
**_Encoding_** is the process of changing data from one form to another. 
