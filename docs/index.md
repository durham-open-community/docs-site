# Welcome

Years ago, I saw a talk by Marcin Jakubowski about creating open source blueprints for fixing farming equipment cheaply. His idea was to create a self-sustaining village. I thought it was an interesting idea. This is before I started a career in technology or even knew what open source software was.  

Although I don't believe we can solution our way to a better world, I have been thinking about how we can use technology in service of the public. So what I want this to be is a place to help communities build and maintain infrastructure to help the distribution of resources through channels not tied to a system that makes it challenging to getting basic needs meet. 

It will be a constantly evolving resource that will take some time. If you would like to get involved then please reach out.

[Email us](mailto:info@duama.org){: .md-button .md-button--primary }
