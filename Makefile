run:
	@docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material

dirty:
	@docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material --dirtyreload --dev-addr=0.0.0.0:8000
		
build:
	@docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material build

.PHONY: run, dirty, build
.EXPORT_ALL_VARIABLES:
