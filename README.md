# Docs Site
Mkdocs site for hosting technical documentation on how to use our tools

## Running locally
There are two was to do this:
- On your local machine using a python virtual environment.
- On your local machine using a container.

### Using a Python virtual environment

- Create and initialize a virtual environment:
```bash
pip install virtualenv

virtualenv -p python3 venv 

. venv/bin/activate
```

- Install packages in virtual environment:
```bash
pip install -r requirements.txt
```

- Run server:
```bash
mkdocs serve
```

### Using a container
Run the following in your terminal:
```bash
make run
```
This will pull the appropriate image and spin up the server. When you make changes to the file and save them it will be reflected in the site. This method can be more resource intensive. 